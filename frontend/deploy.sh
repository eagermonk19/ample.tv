echo "Started: `date`"

aws cloudformation deploy --template-file cloudformation.yaml --stack-name ampletv

set -e

npm run build

aws s3 sync --delete ./build s3://ample.tv

echo "Done: `date`"
