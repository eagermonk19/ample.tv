import axios from "axios";
import { getCookie, RemoveCookie } from "../../util/Utils";

export default async ({ setTwitchToken, setEnableTwitch }) => {
  await axios
    .post(
      `https://id.twitch.tv/oauth2/revoke?client_id=2seu97wcyj3vv9ckedezrwc72iwybf&token=${getCookie("Twitch-access_token")}`
    )
    .catch((er) => {
      console.error(er);
    });

  RemoveCookie("Twitch-access_token");
  RemoveCookie("Twitch-refresh_token");
  RemoveCookie("Twitch-userId");
  RemoveCookie("Twitch-username");
  RemoveCookie("Twitch-profileImg");
  RemoveCookie("TwitchVods_FeedEnabled");
  RemoveCookie("Twitch-myState");
  RemoveCookie("Twitch_AutoRefresh");

  setTwitchToken();
  setEnableTwitch(false);

  await axios
    .delete(`https://o7lhvcvkjg.execute-api.us-east-1.amazonaws.com/Prod/twitch/token`, {
      data: {
        username: getCookie("AioFeed_AccountName"),
        authkey: getCookie(`AioFeed_AuthKey`),
      },
    })
    .then(() => {
      console.log(`Successfully disconnected from Twitch`);
    })
    .catch((e) => {
      console.error(e);
    });
};
