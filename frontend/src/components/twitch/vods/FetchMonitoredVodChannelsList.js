import axios from "axios";

export default async (username, authKey) => {
  return await axios
    .get(`https://o7lhvcvkjg.execute-api.us-east-1.amazonaws.com/Prod/vodchannels`, {
      params: {
        username: username,
        authkey: authKey,
      },
    })
    .then((res) => {
      if (res.data && res.data !== "") {
        return res.data;
      }
      return [];
    })
    .catch((err) => {
      console.error(err);
    });
};
