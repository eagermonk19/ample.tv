import React, { useEffect, useState, useCallback } from "react";
import uniqid from "uniqid";

import ErrorHandler from "./../error";
import { AddCookie } from "../../util/Utils";
import LoadingIndicator from "../LoadingIndicator";

function YoutubeAuth() {
  const [error, setError] = useState();

  const initiateAuth = useCallback(async () => {
    async function generateOrginState() {
      return uniqid();
    }

    const orginState = await generateOrginState();

    AddCookie("Youtube-myState", orginState);

    // window.location.href = `https://accounts.google.com/o/oauth2/v2/auth?client_id=209318310495-g3adrvqfshf4q0536kj0sia00odbrqlb.apps.googleusercontent.com&redirect_uri=http://localhost:3000/youtube/auth&response_type=code&scope=https://www.googleapis.com/auth/youtube.readonly&include_granted_scopes=true&state=${myState.current}`;

    window.location.href = `https://accounts.google.com/o/oauth2/v2/auth?client_id=209318310495-g3adrvqfshf4q0536kj0sia00odbrqlb.apps.googleusercontent.com&redirect_uri=https://ample.tv/auth/youtube/callback&response_type=token&scope=https://www.googleapis.com/auth/youtube&include_granted_scopes=true&state=${orginState}`;
  }, []);

  useEffect(() => {
    (async () => {
      const url = new URL(window.location.href);

      try {
        url.href === "https://ample.tv/auth/youtube"
          ? await initiateAuth()
          : setError({ message: "Visit account page to authendicate with Twitch." });
      } catch (error) {
        setError(error);
      }
    })();
  }, [initiateAuth]);

  if (error) {
    return <ErrorHandler data={error}></ErrorHandler>;
  } else {
    return <LoadingIndicator height={150} width={150} />;
  }
}

export default YoutubeAuth;
